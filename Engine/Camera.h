#pragma once

#include <DirectXMath.h>
#include <glm.hpp>
#include <gtc\constants.hpp>

namespace dsz
{
	class GameTime;

	class Camera
	{
	public:
		glm::vec3 pos;
		glm::vec3 rot;

		glm::vec3 GetForward();
		glm::vec3 GetRight();
		glm::mat4 GetRotationMatrix();
		glm::mat4 GetViewMatrix();

		void SetLookAt(glm::vec3 lookAt, glm::vec3 up);

		void Update(GameTime* gameTime);
	};
}