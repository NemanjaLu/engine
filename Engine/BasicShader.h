#pragma once

#include "D3D12_Shader.h"
#include <DirectXMath.h>
#include <glm.hpp>

DSZ_NAMESPACE_BEGIN

class BasicShader : public D3D12_Shader
{
public:
	struct Vertex
	{
		Vertex() {}

		Vertex(DirectX::XMFLOAT3 position, DirectX::XMFLOAT4 color)
			: position(position), color(color) {}

		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT4 color;
	};

	struct SceneConstantBuffer
	{
		//DirectX::XMMATRIX view;
		glm::mat4 view;
		DirectX::XMMATRIX projection;
	};

	ID3D12Resource* constantBuffer;
	ID3D12DescriptorHeap* constantBufferHeap;
	SceneConstantBuffer* constantBufferData;

	ID3D12RootSignature* CreateRootSignature() override;
	D3D12_INPUT_LAYOUT_DESC GetInputLayoutDesc() override;
	ID3D12DescriptorHeap* GetCBVDescriptorHeaps() override;
	void SetBuffers() override;
	void CreateConstantBuffers() override;
	LPCWSTR GetVSFileName() override;
	LPCWSTR GetPSFileName() override;
};

DSZ_NAMESPACE_END