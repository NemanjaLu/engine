#include "Grid.h"
#include "D3D12_Extensions.h"
#include "D3D12_GraphicsInterface.h"
#include <d3dcompiler.h>
#include <d3d12.h>

DSZ_NAMESPACE_BEGIN

using namespace DirectX;

Grid::Grid(int dimension)
	: dimension(dimension)
{
	basicShader = new BasicShader();
	basicShader->Compile();

	// Vertex Buffer
	vertCount = ((dimension - 2) * 4 + 8);
	bufferSize = vertCount * sizeof(BasicShader::Vertex);

	buffer = new BasicShader::Vertex[vertCount];
	ZeroMemory(buffer, bufferSize);

	buffer[0].position.z = (float)dimension / 2 - 1;
	buffer[0].color = XMFLOAT4(0.f, 0.f, 1.f, 1.f);

	buffer[1].position.z = 0;
	buffer[1].color = XMFLOAT4(0.f, 0.f, 1.f, 1.f);

	buffer[2].position.z = (float)-(dimension / 2 - 1);
	buffer[2].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

	buffer[3].position.z = 0;
	buffer[3].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

	buffer[4].position.x = (float)dimension / 2 - 1;
	buffer[4].color = XMFLOAT4(1.f, 0.f, 0.f, 1.f);

	buffer[5].position.x = 0;
	buffer[5].color = XMFLOAT4(1.f, 0.f, 0.f, 1.f);

	buffer[6].position.x = (float)-(dimension / 2 - 1);
	buffer[6].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

	buffer[7].position.x = 0;
	buffer[7].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

	BasicShader::Vertex* greenLines = buffer + 8;

	for (int i = 0; i < dimension / 2 - 1; ++i)
	{
		greenLines[i * 8 + 0].position.x = (float)(i + 1);
		greenLines[i * 8 + 0].position.z = (float)dimension / 2 - 1;
		greenLines[i * 8 + 0].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

		greenLines[i * 8 + 1].position.x = (float)(i + 1);
		greenLines[i * 8 + 1].position.z = (float)-(dimension / 2 - 1);
		greenLines[i * 8 + 1].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

		greenLines[i * 8 + 2].position.x = (float)-(i + 1);
		greenLines[i * 8 + 2].position.z = (float)dimension / 2 - 1;
		greenLines[i * 8 + 2].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

		greenLines[i * 8 + 3].position.x = (float)-(i + 1);
		greenLines[i * 8 + 3].position.z = (float)-(dimension / 2 - 1);
		greenLines[i * 8 + 3].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

		greenLines[i * 8 + 4].position.x = (float)dimension / 2 - 1;
		greenLines[i * 8 + 4].position.z = (float)(i + 1);
		greenLines[i * 8 + 4].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

		greenLines[i * 8 + 5].position.x = (float)-(dimension / 2 - 1);
		greenLines[i * 8 + 5].position.z = (float)(i + 1);
		greenLines[i * 8 + 5].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

		greenLines[i * 8 + 6].position.x = (float)dimension / 2 - 1;
		greenLines[i * 8 + 6].position.z = (float)-(i + 1);
		greenLines[i * 8 + 6].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);

		greenLines[i * 8 + 7].position.x = (float)-(dimension / 2 - 1);
		greenLines[i * 8 + 7].position.z = (float)-(i + 1);
		greenLines[i * 8 + 7].color = XMFLOAT4(0.f, 1.f, 0.f, 1.f);
	}

	ID3D12Device* dev = currEngineCore->GetGraphicsInterface()->GetDevice();

	dev->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(bufferSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&vb));

	BasicShader::Vertex* vbData;
	vb->Map(0, &CD3DX12_RANGE(0, 0), (void**)&vbData);
	memcpy(vbData, buffer, bufferSize);
	vb->Unmap(0, nullptr);

	vbView.BufferLocation = vb->GetGPUVirtualAddress();
	vbView.StrideInBytes = sizeof(BasicShader::Vertex);
	vbView.SizeInBytes = bufferSize;
}

void Grid::Render()
{
	basicShader->Activate();

	ID3D12GraphicsCommandList* commandList = currEngineCore->GetGraphicsInterface()->GetCommandList();
	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);
	commandList->IASetVertexBuffers(0, 1, &vbView);
	commandList->DrawInstanced(vertCount, 1, 0, 0);
}

DSZ_NAMESPACE_END