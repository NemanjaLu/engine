#pragma once

#include <string>
#include <d3d12.h>
#include <vector>
#include "CoreAPI.h"

DSZ_NAMESPACE_BEGIN

extern std::vector<D3D_FEATURE_LEVEL> FeatureLevels;

std::string FeatureLevelToString(D3D_FEATURE_LEVEL featureLevel);

void PrintAdapterInfo(IDXGIAdapter* adapter, std::wostream& out);

void PrintAdaptersInfo(std::wostream& out);

void PrintDeviceInfo(ID3D12Device* device, std::wostream& out);

D3D_FEATURE_LEVEL GetFeatureLevel(IDXGIAdapter* adapter);

ID3D12Device1* CreateDevice(IDXGIAdapter* adapter, D3D_FEATURE_LEVEL featureLevel);

DSZ_NAMESPACE_END