#include "Input.h"
#include <string.h>
#include <Windows.h>
#include <iostream>

using namespace dsz;

bool Input::keys[256];
bool Input::last_keys[256];
bool Input::mouseButtons[3];

glm::tvec2<int> Input::currentCursorPos;
glm::tvec2<int> Input::lastCursorPos;
bool			Input::bShowCursor = true;
HCURSOR			Input::hCursor = 0;


void Input::Initialize()
{
	memset(keys, false, sizeof(keys));
	memset(last_keys, false, sizeof(last_keys));
	memset(mouseButtons, false, sizeof(mouseButtons));

	hCursor = ::GetCursor();
}

void Input::KeyDown(unsigned int input)
{
	keys[input] = true;
}

void Input::KeyUp(unsigned int input)
{
	keys[input] = false;
}

bool Input::IsKeyDown(Key key)
{
	return keys[(int)key];
}

bool Input::IsKeyPressed(Key key)
{
	return (keys[(int)key] == false && last_keys[(int)key] == true);
}

bool Input::IsButtonDown(MouseButton button)
{
	return mouseButtons[button];
}

void Input::ButtonDown(MouseButton button)
{
	mouseButtons[button] = true;
}

void Input::ButtonUp(MouseButton button)
{
	mouseButtons[button] = false;
}

void Input::Update()
{
	memcpy(last_keys, keys, 256 * sizeof(bool));

	POINT p;
	::GetCursorPos(&p);

	
	lastCursorPos = currentCursorPos;
	currentCursorPos.x = p.x;
	currentCursorPos.y = p.y;
}


glm::vec2 Input::GetCursorPos()
{
	return currentCursorPos;
}

glm::vec2 Input::GetCursorDelta()
{
	return currentCursorPos - lastCursorPos;
}

void Input::SetCursorPos(glm::tvec2<int> pos)
{
	lastCursorPos = pos;
	currentCursorPos = pos;
	::SetCursorPos(pos.x, pos.y);
}

void Input::SetShowCursor(bool showCursor)
{
	if (bShowCursor && !showCursor)
	{
		while (::ShowCursor(false) >= 0);
	}
	else if (!bShowCursor && showCursor)
	{
		while (::ShowCursor(true) < 0);
	}

	bShowCursor = showCursor;
}

bool Input::GetShowCursor()
{
	return bShowCursor;
}