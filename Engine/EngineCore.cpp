#include "EngineCore.h"
#include <iostream>
#include "GameTime.h"
#include "Input.h"
#include "D3D12_GraphicsInterface.h"

// Temp
#include "Grid.h"
#include "Camera.h"

DSZ_NAMESPACE_BEGIN

EngineCore* dsz::currEngineCore = nullptr;
Camera* CurrentCamera = nullptr;

EngineCore::EngineCore()
{
	currEngineCore = this;
	this->hinstance = GetModuleHandle(NULL);
	gameTime = new GameTime();
}

void EngineCore::InitGraphicsInterface(GraphicsInterfaceType graphicsInterfaceType, HWND hwnd /* = 0 */)
{
	switch (graphicsInterfaceType)
	{
		case GraphicsInterfaceType::D3D12:
		{
			graphicsInterface = new D3D12_GraphicsInterface();
			this->hwnd = hwnd;
			if (this->hwnd == 0)
			{
				createWindow();
			}

			graphicsInterface->Create(this->hwnd);
		}
		break;
	}
}


EngineCore::~EngineCore()
{
	destroyWindow();
	delete gameTime;
}

void EngineCore::createWindow()
{
	WNDCLASSEX wc;

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WindowProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hinstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = ENGINE_NAME;
	wc.hIconSm = wc.hIcon;

	RegisterClassEx(&wc);

	this->hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		ENGINE_NAME,
		ENGINE_NAME,
		WS_CLIPCHILDREN | WS_CLIPCHILDREN | WS_POPUP,
		0,
		0,
		RES_X,
		RES_Y,
		NULL,
		NULL,
		hinstance,
		NULL);

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	ShowCursor(true);
}

void EngineCore::destroyWindow()
{
	ShowCursor(true);

	DestroyWindow(hwnd);
	hwnd = NULL;

	UnregisterClass(ENGINE_NAME, hinstance);

	hinstance = NULL;
}

DSZ_ENGINE_API LRESULT CALLBACK WindowProc(
	_In_ HWND   hwnd,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
)
{
	switch (uMsg)
	{
		// Check if the window is being destroyed.
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}

		// Check if the window is being closed.
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}

		// All other messages pass to the message handler in the system class.
		default:
		{
			return currEngineCore->MessageHandler(hwnd, uMsg, wParam, lParam);
		}
	}
}



LRESULT CALLBACK EngineCore::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	switch (umsg)
	{
		// Check if a key has been pressed on the keyboard.
		case WM_SYSKEYDOWN:
		case WM_KEYDOWN:
		{
			// If a key is pressed send it to the input object so it can record that state.
			Input::KeyDown((unsigned int)wparam);
			break;
		}

		// Check if a key has been released on the keyboard.
		case WM_KEYUP:
		{
			// If a key is released then send it to the input object so it can unset the state for that key.
			Input::KeyUp((unsigned int)wparam);
			break;
		}

		case WM_LBUTTONDOWN:
		{
			Input::ButtonDown(LEFT);
			break;
		}

		case WM_MBUTTONDOWN:
		{
			Input::ButtonDown(MIDDLE);
			break;
		}

		case WM_RBUTTONDOWN:
		{
			Input::ButtonDown(RIGHT);
			break;
		}

		case WM_LBUTTONUP:
		{
			Input::ButtonUp(LEFT);
			break;
		}

		case WM_MBUTTONUP:
		{
			Input::ButtonUp(MIDDLE);
			break;
		}

		case WM_RBUTTONUP:
		{
			Input::ButtonUp(RIGHT);
			break;
		}

		case WM_MOUSEWHEEL:
		{
			//Input::scrollValue = (float)(GET_WHEEL_DELTA_WPARAM(wparam));
			break;
		}

		// Any other messages send to the default message handler as our application won't make use of them.
		default:
		{
			return DefWindowProc(hwnd, umsg, wparam, lparam);
		}
	}
}

void CheckMessages()
{

}

void EngineCore::gameLoop()
{
	MSG msg;

	Grid grid(100);

	while (!bShutdownRequest)
	{
		LARGE_INTEGER currentTime;
		QueryPerformanceCounter(&currentTime);

		this->gameTime->currentTime = currentTime;

		double elapsed = gameTime->dt();

		if (elapsed >= 1 / TARGET_FPS)
		{
			Input::Update();

			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			if (Input::IsKeyPressed(Key::ESC))
				Shutdown();

			CurrentCamera->Update(gameTime);

			graphicsInterface->BeginScene();
			grid.Render();
			graphicsInterface->EndScene();

			this->gameTime->lastUpdateTime = this->gameTime->currentTime;
		}
	}
}

void EngineCore::Start()
{
	gameLoop();
}

void EngineCore::Shutdown()
{
	bShutdownRequest = true;
}

DSZ_NAMESPACE_END