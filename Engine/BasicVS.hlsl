cbuffer SceneConstantBuffer : register(b0)
{
    matrix view;
    matrix projection;
}

struct PSInput
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

PSInput main(float4 position : POSITION, float4 color : COLOR)
{
    PSInput result;

    result.position = mul(position, mul(view, projection));
    result.color = color;

    return result;
}