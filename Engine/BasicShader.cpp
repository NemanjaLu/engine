#include "BasicShader.h"
#include "DSZ_Math.h"
#include "Camera.h"

DSZ_NAMESPACE_BEGIN

ID3D12RootSignature* BasicShader::CreateRootSignature()
{
	CD3DX12_DESCRIPTOR_RANGE1 range;
	range.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV,
		1,
		0,
		0,
		D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC);

	CD3DX12_ROOT_PARAMETER1 rootParameter;
	rootParameter.InitAsDescriptorTable(1, &range, D3D12_SHADER_VISIBILITY_VERTEX);

	D3D12_VERSIONED_ROOT_SIGNATURE_DESC rsd;
	rsd.Desc_1_1.NumParameters = 1;
	rsd.Desc_1_1.pParameters = &rootParameter;
	rsd.Desc_1_1.NumStaticSamplers = 0;
	rsd.Desc_1_1.pStaticSamplers = nullptr;
	rsd.Desc_1_1.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
	rsd.Version = D3D_ROOT_SIGNATURE_VERSION_1_1;

	ID3DBlob* signature;
	ID3DBlob* error;
	D3D12SerializeVersionedRootSignature(&rsd, &signature, &error);

	ID3D12Device* device = currEngineCore->GetGraphicsInterface()->GetDevice();
	ID3D12RootSignature* rootSignature = nullptr;
	HRESULT res = device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&rootSignature));

	return rootSignature;
}

static D3D12_INPUT_ELEMENT_DESC inputElementDesc[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
};

D3D12_INPUT_LAYOUT_DESC BasicShader::GetInputLayoutDesc()
{
	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;

	inputLayoutDesc.pInputElementDescs = inputElementDesc;
	inputLayoutDesc.NumElements = _countof(inputElementDesc);

	return inputLayoutDesc;
}

void BasicShader::CreateConstantBuffers()
{
	ID3D12Device* device = currEngineCore->GetGraphicsInterface()->GetDevice();

	D3D12_DESCRIPTOR_HEAP_DESC cbvHeapDesc = {};
	cbvHeapDesc.NumDescriptors = 1;
	cbvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	device->CreateDescriptorHeap(&cbvHeapDesc, IID_PPV_ARGS(&constantBufferHeap));

	device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(sizeof(SceneConstantBuffer)),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&constantBuffer)
	);

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc = {};
	cbvDesc.BufferLocation = constantBuffer->GetGPUVirtualAddress();
	cbvDesc.SizeInBytes = (sizeof(SceneConstantBuffer) + 255) & ~255;
	device->CreateConstantBufferView(&cbvDesc, constantBufferHeap->GetCPUDescriptorHandleForHeapStart());

	CD3DX12_RANGE readRangee(0, 0);
	constantBuffer->Map(0, &readRangee, (void**)&constantBufferData);
}

ID3D12DescriptorHeap* BasicShader::GetCBVDescriptorHeaps()
{
	return constantBufferHeap;
}

void BasicShader::SetBuffers()
{
	using namespace DirectX;

	//XMMATRIX view = DirectX::XMMatrixLookAtLH(ToXMVector(CurrentCamera->position), ToXMVector(CurrentCamera->lookAt), ToXMVector(XMFLOAT4(0.f, 1.f, 0.f, 1.f)));
	XMMATRIX projection = DirectX::XMMatrixPerspectiveFovLH(XM_PIDIV4, 800.f / 600.f, 0.1f, 100.f);
	//view = XMMatrixTranspose(view);

	

	glm::mat4 mat = CurrentCamera->GetViewMatrix();

	//constantBufferData->view = XMMatrixTranspose(view);
	constantBufferData->view = glm::transpose(mat);
	constantBufferData->projection = XMMatrixTranspose(projection);
}

LPCWSTR BasicShader::GetVSFileName()
{
	return L"../Engine/BasicVS.hlsl";
}

LPCWSTR BasicShader::GetPSFileName()
{
	return L"../Engine/BasicPS.hlsl";
}

DSZ_NAMESPACE_END