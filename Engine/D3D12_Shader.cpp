#include "D3D12_Shader.h"

DSZ_NAMESPACE_BEGIN

void D3D12_Shader::Compile()
{
	rootSignature = CreateRootSignature();
	inputLayoutDesc = GetInputLayoutDesc();
	CreateConstantBuffers();

	ID3DBlob* vs;
	ID3DBlob* ps;

	HRESULT res = D3DCompileFromFile(GetVSFileName(), nullptr, nullptr, "main", "vs_5_0", 0, 0, &vs, nullptr);
	res = D3DCompileFromFile(GetPSFileName(), nullptr, nullptr, "main", "ps_5_0", 0, 0, &ps, nullptr);

	// PSO
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = inputLayoutDesc;
	psoDesc.pRootSignature = rootSignature;
	psoDesc.VS = CD3DX12_SHADER_BYTECODE(vs);
	psoDesc.PS = CD3DX12_SHADER_BYTECODE(ps);
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState.DepthEnable = TRUE;
	psoDesc.DepthStencilState.StencilEnable = FALSE;
	psoDesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	psoDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;

	ID3D12Device* device = currEngineCore->GetGraphicsInterface()->GetDevice();
	res = device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&pso));

	viewport.Width = RES_X;
	viewport.Height = RES_Y;
	viewport.MaxDepth = 1.0f;

	scissorRect.right = RES_X;
	scissorRect.bottom = RES_Y;
}

void D3D12_Shader::Activate()
{
	ID3D12Device* device = currEngineCore->GetGraphicsInterface()->GetDevice();
	ID3D12CommandAllocator* commandAllocator = currEngineCore->GetGraphicsInterface()->GetCommandAllocator();
	ID3D12GraphicsCommandList* commandList = currEngineCore->GetGraphicsInterface()->GetCommandList();

	SetBuffers();
	ID3D12DescriptorHeap* cbvDescriptorHeaps = GetCBVDescriptorHeaps();

	commandList->SetPipelineState(pso);
	commandList->SetGraphicsRootSignature(rootSignature);
	commandList->SetDescriptorHeaps(1, &cbvDescriptorHeaps);
	commandList->SetGraphicsRootDescriptorTable(0, cbvDescriptorHeaps->GetGPUDescriptorHandleForHeapStart());
	commandList->RSSetViewports(1, &viewport);
	commandList->RSSetScissorRects(1, &scissorRect);

	
}

DSZ_NAMESPACE_END