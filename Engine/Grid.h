#pragma once

#include "CoreAPI.h"
#include <d3d12.h>
#include <DirectXMath.h>
#include <vector>
#include "BasicShader.h"

DSZ_NAMESPACE_BEGIN

class Grid
{
public:
	Grid(int dimension);

	void ChangeDimension(int newDimension);

	void Render();

private:
	int dimension;

	int	vertCount;
	int	bufferSize;
	BasicShader::Vertex* buffer;

	ID3D12Resource*				vb;
	D3D12_VERTEX_BUFFER_VIEW	vbView;

	BasicShader* basicShader = nullptr;
};

DSZ_NAMESPACE_END
