#pragma once

#include "System.h"
#include <glm.hpp>
#include <Windows.h>

DSZ_NAMESPACE_BEGIN

enum MouseButton { LEFT = 0, MIDDLE, RIGHT };
enum class Key
{
	CTRL = VK_CONTROL,
	ESC = 0x1B,
	SPACE = 0x20,
	LEFT = 0x25, UP, RIGHT, DOWN,
	A = 0x41, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
	F1 = 0x70, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12
};

class Input
{
public:
	static void Update();

	static bool IsKeyDown(Key key);
	static bool IsButtonDown(MouseButton button);
	static bool IsKeyPressed(Key key);

	static glm::vec2 GetCursorPos();
	static glm::vec2 GetCursorDelta();
	static void SetCursorPos(glm::tvec2<int> pos);
	static void SetShowCursor(bool showCursor);
	static bool GetShowCursor();


private:
	static void Initialize();

	DSZ_ENGINE_API static void KeyDown(unsigned int input);
	DSZ_ENGINE_API static void KeyUp(unsigned int input);

	static void ButtonDown(MouseButton button);
	static void ButtonUp(MouseButton button);

private:
	static bool keys[256], last_keys[256];
	static bool mouseButtons[3];

	static glm::tvec2<int>	lastCursorPos, currentCursorPos;
	static HCURSOR			hCursor;
	static bool				bShowCursor;


private:
	Input();
	Input(const Input&);

	friend class EngineCore;
	friend class GameViewport;
};

DSZ_NAMESPACE_END