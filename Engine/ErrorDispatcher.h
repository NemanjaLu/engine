#ifndef __ERROR_DISPATCHER_H__
#define __ERROR_DISPATCHER_H__

#include <string>
#include "System.h"

DSZ_NAMESPACE_BEGIN

namespace logger
{

    enum class LogLevel
    {
        Debug,
        Info,
        Warning,
        Error,
        None
    };

    const char* LogLevelToCString(LogLevel logLevel);

    enum class MessageType
    {
        MessageType_Info,
        MessageType_Assert
    };

    class LogCategory
    {
    public:
        LogCategory(const char* name, const LogLevel& logLevel = LogLevel::None, bool isEnabled = true);

        const char* name;
        const LogLevel logLevel;
        bool isEnabled;
    };

    class ErrorDispatcher
    {
    public:
        ErrorDispatcher();
        virtual ~ErrorDispatcher();

        virtual void LogDebug(const LogCategory& logCategory, const char* message, const char* file, int line) = 0;
        virtual void LogInfo(const LogCategory& logCategory, const char* message, const char* file, int line) = 0;
        virtual void LogWarning(const LogCategory& logCategory, const char* message, const char* file, int line) = 0;
        virtual void LogError(const LogCategory& logCategory, const char* message, const char* file, int line) = 0;
    protected:
        static std::string GetCurrentTimeString();
    };

    class ConsoleErrorDispatcher : public ErrorDispatcher
    {
    public:
        virtual ~ConsoleErrorDispatcher() {}

        virtual void LogDebug(const LogCategory& logCategory, const char* message, const char* file, int line) override;
        virtual void LogInfo(const LogCategory& logCategory, const char* message, const char* file, int line) override;
        virtual void LogWarning(const LogCategory& logCategory, const char* message, const char* file, int line) override;
        virtual void LogError(const LogCategory& logCategory, const char* message, const char* file, int line) override;
    protected:
        virtual void Output(LogLevel logLevel, const LogCategory& logCategory, const char* message, const char* file, int line);
    };

    extern ConsoleErrorDispatcher g_ConsoleErrorDispatcher;

    class FileErrorDispatcher : public ErrorDispatcher
    {
    public:
        FileErrorDispatcher(const std::string& logDirectoryName);
        virtual void LogDebug(const LogCategory& logCategory, const char* message, const char* file, int line) override;
        virtual void LogInfo(const LogCategory& logCategory, const char* message, const char* file, int line) override;
        virtual void LogWarning(const LogCategory& logCategory, const char* message, const char* file, int line) override;
        virtual void LogError(const LogCategory& logCategory, const char* message, const char* file, int line) override;
    protected:
        virtual void Output(LogLevel logLevel, const LogCategory& logCategory, const char* message, const char* file, int line);
        std::string m_LogDirectoryName;
    };

    extern FileErrorDispatcher g_FileErrorDispatcher;

}

DSZ_NAMESPACE_END

#endif