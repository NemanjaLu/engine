#pragma once

#include "CoreAPI.h"
#include <DirectXMath.h>

DSZ_NAMESPACE_BEGIN

using namespace DirectX;

DirectX::XMVECTOR ToXMVector(DirectX::XMFLOAT4 f);

DirectX::XMVECTOR ToXMVector(DirectX::XMFLOAT3 f);

DSZ_NAMESPACE_END