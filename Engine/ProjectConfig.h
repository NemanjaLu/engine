#ifndef __PROJECT_CONFIG_H__
#define __PROJECT_CONFIG_H__

#if !defined(DSZ_FINAL)
#define DSZ_USE_LOGSYSTEM
#endif

#if !defined(DSZ_FINAL)
#define DSZ_USE_PROFILER
#endif

#if !defined(DSZ_FINAL)
#define DSZ_USE_METADATA
#endif

#endif // #ifndef __PROJECT_CONFIG_H__