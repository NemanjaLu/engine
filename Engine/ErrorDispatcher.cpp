#include "ErrorDispatcher.h"

#include <chrono>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <sstream>

#include "ErrorDispatcherManager.h"

DSZ_NAMESPACE_BEGIN

namespace logger
{

    const char* LogLevelToCString(LogLevel logLevel)
    {
        switch (logLevel)
        {
            case logger::LogLevel::None: return "None";
            case logger::LogLevel::Error: return "Error";
            case logger::LogLevel::Warning: return "Warning";
            case logger::LogLevel::Info: return "Info";
            case logger::LogLevel::Debug: return "Debug";

            default: return "";
        }
    }

    LogCategory::LogCategory(const char* name, const LogLevel& logLevel, bool isEnabled)
        : name(name)
        , logLevel(logLevel)
        , isEnabled(isEnabled)
    {}



    // ErrorDispatcher

    ErrorDispatcher::ErrorDispatcher()
    {
        g_ErrorDispatcherManager.RegisterErrorDispatcher(this);
    }

    ErrorDispatcher::~ErrorDispatcher()
    {
        g_ErrorDispatcherManager.UnregisterErrorDispatcher(this);
    }

    std::string ErrorDispatcher::GetCurrentTimeString()
    {
        std::chrono::high_resolution_clock::time_point timePoint = std::chrono::high_resolution_clock::now();

        std::chrono::hours          timeHours   = std::chrono::duration_cast<std::chrono::hours         >(timePoint.time_since_epoch());
        std::chrono::minutes        timeMinutes = std::chrono::duration_cast<std::chrono::minutes       >(timePoint.time_since_epoch() - timeHours);
        std::chrono::seconds        timeSeconds = std::chrono::duration_cast<std::chrono::seconds       >(timePoint.time_since_epoch() - timeMinutes - timeHours);
        std::chrono::milliseconds   timeMillis  = std::chrono::duration_cast<std::chrono::milliseconds  >(timePoint.time_since_epoch() - timeSeconds - timeMinutes - timeHours);

        std::stringstream timeStringStream;
        timeStringStream
            << std::setw(2) << std::setfill('0') << timeHours.count()
            << ":"
            << std::setw(2) << std::setfill('0') << timeMinutes.count()
            << ":"
            << std::setw(2) << std::setfill('0') << timeSeconds.count()
            << " ("
            << std::setw(4) << std::setfill('0') << timeMillis.count()
            << ")";
        const std::string timeStr = timeStringStream.str();

        return timeStr;
    }



    // ConsoleErrorDispatcher

    void ConsoleErrorDispatcher::LogDebug(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        Output(LogLevel::Debug, logCategory, message, file, line);
    }

    void ConsoleErrorDispatcher::LogInfo(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        Output(LogLevel::Info, logCategory, message, file, line);
    }

    void ConsoleErrorDispatcher::LogWarning(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        Output(LogLevel::Warning, logCategory, message, file, line);
    }

    void ConsoleErrorDispatcher::LogError(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        Output(LogLevel::Error, logCategory, message, file, line);
    }

    void ConsoleErrorDispatcher::Output(LogLevel logLevel, const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        if (logCategory.isEnabled == false || logLevel < logCategory.logLevel || logLevel == LogLevel::None)
        {
            return;
        }

        fprintf(stdout, "[%s][%s][%s][file:%s (%d)]: %s\n", GetCurrentTimeString().c_str(), logCategory.name, LogLevelToCString(logLevel), file, line, message);
    }

    ConsoleErrorDispatcher g_ConsoleErrorDispatcher;



    // FileErrorDispatcher

    FileErrorDispatcher::FileErrorDispatcher(const std::string& logDirectoryName)
        : m_LogDirectoryName(logDirectoryName)
    {
        if (m_LogDirectoryName.size() == 0)
        {
            m_LogDirectoryName = "..\\";
        }
        else if (m_LogDirectoryName.back() != '\\')
        {
            m_LogDirectoryName += '\\';
        }
    }

    void FileErrorDispatcher::LogDebug(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        Output(LogLevel::Debug, logCategory, message, file, line);
    }

    void FileErrorDispatcher::LogInfo(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        Output(LogLevel::Info, logCategory, message, file, line);
    }

    void FileErrorDispatcher::LogWarning(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        Output(LogLevel::Warning, logCategory, message, file, line);
    }

    void FileErrorDispatcher::LogError(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        Output(LogLevel::Error, logCategory, message, file, line);
    }

    void FileErrorDispatcher::Output(LogLevel logLevel, const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        if (logCategory.isEnabled == false || logLevel < logCategory.logLevel || logLevel == LogLevel::None)
        {
            return;
        }

        std::ofstream outFile;
        outFile.open(m_LogDirectoryName + logCategory.name + ".log", std::ios::out | std::ios::app);
        if (outFile.is_open() == false)
        {
            fprintf(stderr, "Could not open log file \"%s\"", (m_LogDirectoryName + logCategory.name).c_str());
            return;
        }

        outFile
            << "[" << GetCurrentTimeString() << "]"
            << "[" << LogLevelToCString(logLevel) << "]"
            << "[file:" << file << " (" << line << ")]: "
            << message
            << std::endl;

        outFile.close();
    }

    FileErrorDispatcher g_FileErrorDispatcher{ "Logs" };

}

DSZ_NAMESPACE_END