#ifndef __SYSTEM_H__
#define __SYSTEM_H__

enum EIsInDSZNamespace { Value = 0 };
namespace dsz { enum EIsInDSZNamespace { Value = 1 }; }

#define DSZ_NAMESPACE_BEGIN                                             \
static_assert(EIsInDSZNamespace::Value == 0, "Nested dsz namespace");   \
namespace dsz {

#define DSZ_NAMESPACE_END                                               \
static_assert(EIsInDSZNamespace::Value == 1, "Not in dsz namespace");   \
}

#define STRINGIZE(value)                    #value
#define MAKE_STRING(stringizator, value)    stringizator(value)
#define $Line                               MAKE_STRING(STRINGIZE, __LINE__)
#define PRAGMA_MESSAGE_HEADER               __FILE__ "(" $Line ") : "
#define TODO_MESSAGE_HEADER PRAGMA_MESSAGE_HEADER "TODO: "
#define NOTE_MESSAGE_HEADER PRAGMA_MESSAGE_HEADER "NOTE: "

// Usage:
// #pragma TODO("This needs to be done.")
// #pragma NOTE("This is a remainder in compile time.")
#define TODO(value) message(TODO_MESSAGE_HEADER MAKE_STRING(STRINGIZE, value))
#define NOTE(value) message(NOTE_MESSAGE_HEADER MAKE_STRING(STRINGIZE, value))

//#ifdef DSZ_DEBUG
//#define ASSUME(expr) assert(expr)
//#else
//#define ASSUME(expr) assume(expr)
//#endif

#ifdef DSZ_ENGINE_EXPORT
#define DSZ_ENGINE_API __declspec(dllexport)
#else
#define DSZ_ENGINE_API __declspec(dllimport)
#endif

#endif // #ifndef __SYSTEM_H__