#pragma once

#include "EngineCore.h"

namespace dsz
{
	void DSZ_ENGINE_API ConsoleCreate();
	extern DSZ_ENGINE_API void ConsoleCleanup();
}