#pragma once

#include "GraphicsInterface.h"

#include <Windows.h>
#include <d3d12.h>
#include <dxgi1_4.h>

namespace dsz
{
	class D3D12_GraphicsInterface : public GraphicsInterface
	{
	public:
		void Create(HWND hwnd) override;
		void Destroy() override;

		ID3D12Device*				GetDevice() { return m_device; }
		ID3D12CommandQueue*			GetCommandQueue() { return m_commandQueue; }
		ID3D12CommandAllocator*		GetCommandAllocator() { return m_commandAllocator; }
		ID3D12GraphicsCommandList*	GetCommandList() { return m_commandList; }

		void SetFullScreen(bool bFullscreen) override;

		void BeginScene() override;
		void EndScene() override;

		void WaitForPreviousFrame();

	private:
		D3D12_RESOURCE_BARRIER barrier;

		ID3D12Device* m_device;
		ID3D12CommandQueue* m_commandQueue;
		IDXGISwapChain3* m_swapChain;

		ID3D12DescriptorHeap* m_renderTargetViewHeap;
		ID3D12Resource* m_backBufferRenderTarget[2];
		unsigned int m_bufferIndex;

		ID3D12DescriptorHeap* m_depthStencilHeap;
		ID3D12Resource* m_depthStencilBuffer;

		ID3D12CommandAllocator* m_commandAllocator;
		ID3D12GraphicsCommandList* m_commandList;
		ID3D12PipelineState* m_pipelineState;
		ID3D12Fence* m_fence;
		HANDLE m_fenceEvent;
		unsigned long long m_fenceValue;
	};
}