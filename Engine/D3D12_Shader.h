#pragma once

#include "CoreAPI.h"
#include <d3d12.h>
#include <d3dcompiler.h>
#include "D3D12_Extensions.h"
#include "EngineCore.h"
#include "D3D12_GraphicsInterface.h"

DSZ_NAMESPACE_BEGIN

class D3D12_Shader
{
public:
	void Compile();

	void Activate();
	//ID3D12GraphicsCommandList* GetCommandList() { return commandList; }

protected:
	virtual ID3D12RootSignature* CreateRootSignature() = 0;
	virtual D3D12_INPUT_LAYOUT_DESC GetInputLayoutDesc() = 0;
	virtual ID3D12DescriptorHeap* GetCBVDescriptorHeaps() = 0;
	virtual void SetBuffers() = 0;
	virtual void CreateConstantBuffers() = 0;
	virtual LPCWSTR GetVSFileName() = 0;
	virtual LPCWSTR GetPSFileName() = 0;

private:
	ID3D12RootSignature* rootSignature = nullptr;
	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	ID3D12PipelineState* pso;
	//ID3D12GraphicsCommandList* commandList = nullptr;

	D3D12_VIEWPORT	viewport;
	D3D12_RECT		scissorRect;

private:
	
};

DSZ_NAMESPACE_END