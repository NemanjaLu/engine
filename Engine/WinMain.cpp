#include <Windows.h>
#include <iostream>
#include <io.h>
#include <fcntl.h>
#include "EngineCore.h"
#include "Globals.h"

using namespace dsz;

void dsz::ConsoleCreate();
void dsz::ConsoleCleanup();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{

	ConsoleCreate();
	EngineCore* engine = new EngineCore();
	engine->InitGraphicsInterface(GraphicsInterfaceType::D3D12);
	engine->Start();

	delete engine;
	ConsoleCleanup();

	return 0;
}
