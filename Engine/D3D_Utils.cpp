#include "D3D_Utils.h"
#include "CoreAPI.h"
#include <vector>
#include <dxgi1_4.h>

DSZ_NAMESPACE_BEGIN

std::vector<D3D_FEATURE_LEVEL> FeatureLevels =
{
	D3D_FEATURE_LEVEL_12_1,
	D3D_FEATURE_LEVEL_12_0,
	D3D_FEATURE_LEVEL_11_1,
	D3D_FEATURE_LEVEL_11_0,
	D3D_FEATURE_LEVEL_10_1,
	D3D_FEATURE_LEVEL_10_0,
	D3D_FEATURE_LEVEL_9_3,
	D3D_FEATURE_LEVEL_9_2,
	D3D_FEATURE_LEVEL_9_1
};


std::string FeatureLevelToString(D3D_FEATURE_LEVEL featureLevel)
{
	switch (featureLevel)
	{
		case D3D_FEATURE_LEVEL_12_1:
			return "12_1";
			break;
		case D3D_FEATURE_LEVEL_12_0:
			return "12_0";
			break;
		case D3D_FEATURE_LEVEL_11_1:
			return "11_1";
			break;
		case D3D_FEATURE_LEVEL_11_0:
			return "11_0";
			break;
		case D3D_FEATURE_LEVEL_10_1:
			return "10_1";
			break;
		case D3D_FEATURE_LEVEL_10_0:
			return "10_0";
			break;
		case D3D_FEATURE_LEVEL_9_3:
			return "9_3";
			break;
		case D3D_FEATURE_LEVEL_9_2:
			return "9_2";
			break;
		case D3D_FEATURE_LEVEL_9_1:
			return "9_1";
			break;
		default:
			return "Invalid Feature Level";
	}
}

void PrintAdaptersInfo(std::wostream& out)
{
	IDXGIFactory4* factory;
	CreateDXGIFactory1(__uuidof(IDXGIFactory4), (void**)&factory);

	IDXGIAdapter* adapter;

	out << "*************** Present adapters *****************" << std::endl;

	for (UINT i = 0; factory->EnumAdapters(i, &adapter) != DXGI_ERROR_NOT_FOUND; ++i)
	{

		PrintAdapterInfo(adapter, out);
		out << std::endl;

		adapter->Release();
	}

	out << "**************************************************";

	factory->Release();
}

void PrintAdapterInfo(IDXGIAdapter* adapter, std::wostream& out)
{
	DXGI_ADAPTER_DESC adapterDesc;
	adapter->GetDesc(&adapterDesc);

	out << "Description : " << adapterDesc.Description << std::endl;
	for (UINT i = 0; i < dsz::FeatureLevels.size(); ++i)
	{
		if (SUCCEEDED(D3D12CreateDevice(adapter, dsz::FeatureLevels[i], __uuidof(ID3D12Device), nullptr)))
		{
			out << "Max feature level : " << FeatureLevelToString(FeatureLevels[i]).c_str() << std::endl;
			break;
		}
	}
}

D3D_FEATURE_LEVEL GetFeatureLevel(IDXGIAdapter* adapter)
{
	for (UINT i = 0; i < dsz::FeatureLevels.size(); ++i)
	{
		if (SUCCEEDED(D3D12CreateDevice(adapter, dsz::FeatureLevels[i], __uuidof(ID3D12Device), nullptr)))
		{
			return dsz::FeatureLevels[i];
		}
	}

	return D3D_FEATURE_LEVEL_9_1;
}

ID3D12Device1* CreateDevice(IDXGIAdapter* adapter, D3D_FEATURE_LEVEL featureLevel)
{
	ID3D12Device1* device = nullptr;
	D3D12CreateDevice(adapter, featureLevel, IID_PPV_ARGS(&device));

	return device;
}

void PrintDeviceInfo(ID3D12Device* device, std::wostream& out)
{
	IDXGIFactory4* factory;
	CreateDXGIFactory2(0, IID_PPV_ARGS(&factory));

	LUID adapter_luid = device->GetAdapterLuid();

	IDXGIAdapter* used_adapter = nullptr;
	factory->EnumAdapterByLuid(adapter_luid, IID_PPV_ARGS(&used_adapter));

	PrintAdapterInfo(used_adapter, out);

	used_adapter->Release();

	factory->Release();
}

DSZ_NAMESPACE_END