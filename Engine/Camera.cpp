#include "Camera.h"
#include "Input.h"
#include <gtx\transform.hpp>
#include "GameTime.h"
#include <gtc\quaternion.hpp>
#include <iostream>
#include <gtx\rotate_vector.hpp>


DSZ_NAMESPACE_BEGIN

using namespace DirectX;
using namespace glm;

void Camera::Update(GameTime* gameTime)
{
	float speed = 15.f;
	float sensitivity = 10.f;

	if (Input::IsButtonDown(MouseButton::RIGHT))
	{
		Input::SetShowCursor(false);

		if (Input::IsKeyDown(Key::W))
		{
			pos += GetForward() * (float)gameTime->dt() * speed;
		}

		if (Input::IsKeyDown(Key::S))
		{
			pos -= GetForward() * (float)gameTime->dt() * speed;
		}

		if (Input::IsKeyDown(Key::A))
		{
			pos += GetRight() * (float)gameTime->dt() * speed;
		}

		if (Input::IsKeyDown(Key::D))
		{
			pos -= GetRight() * (float)gameTime->dt() * speed;
		}

		if (Input::IsKeyDown(Key::SPACE))
		{
			pos.y += (float)gameTime->dt() * speed;
		}

		if (Input::IsKeyDown(Key::CTRL))
		{
			pos.y -= (float)gameTime->dt() * speed;
		}

		rot.x += Input::GetCursorDelta().y * sensitivity * (float)gameTime->dt();
		rot.y += Input::GetCursorDelta().x * sensitivity * (float)gameTime->dt();

		
	}
	else if (Input::IsButtonDown(MouseButton::MIDDLE))
	{
		Input::SetShowCursor(false);
		pos += Input::GetCursorDelta().x * GetRight() * speed * (float)gameTime->dt();
		pos.y += Input::GetCursorDelta().y * speed * (float)gameTime->dt();
		Input::SetCursorPos(vec2(400, 300));
	}
	else
	{
		Input::SetShowCursor(true);
	}

}

glm::mat4 Camera::GetRotationMatrix()
{
	return rotate(rot.z, vec3(0.f, 0.f, 1.f)) * rotate(rot.y, vec3(0.f, 1.f, 0.f)) * rotate(rot.x, vec3(1.f, 0.f, 0.f));
}

glm::mat4 Camera::GetViewMatrix()
{
	mat4 trMat = translate(pos);

	return inverse(trMat * GetRotationMatrix());
}

glm::vec3 Camera::GetForward()
{
	return GetRotationMatrix() * vec4(0.f, 0.f, 1.f, 1.f);
}

glm::vec3 Camera::GetRight()
{
	return GetRotationMatrix() * vec4(-1.f, 0.f, 0.f, 1.f);
}

void Camera::SetLookAt(glm::vec3 lookAt, glm::vec3 up)
{
	vec3 z = normalize(lookAt - pos);
	vec3 x = normalize(cross(up, z));
	vec3 y = cross(z, x);

	mat4 rotMat;
	
	rotMat[0] = vec4(x, 0.f);
	rotMat[1] = vec4(y, 0.f);
	rotMat[2] = vec4(z, 0.f);

	float PI = pi<float>();
	
	float ay = PI + asinf(rotMat[0][2]);

	float cosy = cosf(ay);
	float ax = atan2f(rotMat[1][2] / cosy, rotMat[2][2] / cosy);
	float az = atan2f(rotMat[0][1] / cosy, rotMat[0][0] / cosy);

	rot.x = ax;
	rot.y = ay;
	rot.z = az;
}

DSZ_NAMESPACE_END