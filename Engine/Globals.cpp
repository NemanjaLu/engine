#include "Globals.h"
#include <Windows.h>
#include <iostream>
#include <io.h>
#include <fcntl.h>

using namespace dsz;

void dsz::ConsoleCreate()
{
	AllocConsole();
	FILE* conout;
	FILE* conin;

	freopen_s(&conout, "CONOUT$", "w", stdout);
	freopen_s(&conin, "CONIN$", "r", stdin);

	SetWindowPos(GetConsoleWindow(), 0, 900, 0, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
}

void dsz::ConsoleCleanup()
{
	FreeConsole();
}