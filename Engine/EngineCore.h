#ifndef __ENGINE_CORE_H__
#define __ENGINE_CORE_H__

#include <Windows.h>
#include "System.h"

DSZ_NAMESPACE_BEGIN

#define ENGINE_NAME L"KITA"
#define RES_X 800
#define RES_Y 600
#define TARGET_FPS 60

class GameTime;
class EngineCore;
class D3D12_GraphicsInterface;
class Camera;

enum class GraphicsInterfaceType { D3D12 };

extern DSZ_ENGINE_API EngineCore* currEngineCore;
extern Camera* CurrentCamera;

class EngineCore
{
public:
	DSZ_ENGINE_API EngineCore();
	DSZ_ENGINE_API ~EngineCore();

	DSZ_ENGINE_API void InitGraphicsInterface(GraphicsInterfaceType graphicsInterfaceType, HWND hwnd = 0);

	DSZ_ENGINE_API void Start();
	DSZ_ENGINE_API void Shutdown();

	DSZ_ENGINE_API HWND GetHwnd() { return hwnd; }
	D3D12_GraphicsInterface* GetGraphicsInterface() { return graphicsInterface; }

private:
	HWND hwnd;
	HINSTANCE hinstance;
	D3D12_GraphicsInterface* graphicsInterface;

	GameTime* gameTime;

	bool bShutdownRequest = false;
private:
	void gameLoop();

	void createWindow();
	void destroyWindow();

	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

	
	friend DSZ_ENGINE_API LRESULT CALLBACK WindowProc(
		_In_ HWND   hwnd,
		_In_ UINT   uMsg,
		_In_ WPARAM wParam,
		_In_ LPARAM lParam
	);

};

DSZ_ENGINE_API LRESULT CALLBACK WindowProc(
	_In_ HWND   hwnd,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
);

DSZ_NAMESPACE_END

#endif