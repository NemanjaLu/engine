#include "ErrorDispatcherManager.h"


DSZ_NAMESPACE_BEGIN

namespace logger
{

    void ErrorDispatcherManager::RegisterErrorDispatcher(ErrorDispatcher* errorDispatcher)
    {
        if (errorDispatcher == nullptr)
        {
            return;
        }

        m_ErrorDispatchers.push_back(errorDispatcher);
    }

    void ErrorDispatcherManager::UnregisterErrorDispatcher(ErrorDispatcher* errorDispatcher)
    {
        for (size_t i = 0; i < m_ErrorDispatchers.size(); ++i)
        {
            if (m_ErrorDispatchers[i] == errorDispatcher)
            {
                for (size_t j = i + 1; j < m_ErrorDispatchers.size(); ++j)
                {
                    m_ErrorDispatchers[j - 1] = m_ErrorDispatchers[j];
                    m_ErrorDispatchers.pop_back();
                }
            }
        }
    }

    void ErrorDispatcherManager::LogDebug(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        for (size_t i = 0; i < m_ErrorDispatchers.size(); ++i)
        {
            ErrorDispatcher* const errorDispatcher = m_ErrorDispatchers[i];
            errorDispatcher->LogDebug(logCategory, message, file, line);
        }
    }

    void ErrorDispatcherManager::LogInfo(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        for (size_t i = 0; i < m_ErrorDispatchers.size(); ++i)
        {
            ErrorDispatcher* const errorDispatcher = m_ErrorDispatchers[i];
            errorDispatcher->LogInfo(logCategory, message, file, line);
        }
    }

    void ErrorDispatcherManager::LogWarning(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        for (size_t i = 0; i < m_ErrorDispatchers.size(); ++i)
        {
            ErrorDispatcher* const errorDispatcher = m_ErrorDispatchers[i];
            errorDispatcher->LogWarning(logCategory, message, file, line);
        }
    }

    void ErrorDispatcherManager::LogError(const LogCategory& logCategory, const char* message, const char* file, int line)
    {
        for (size_t i = 0; i < m_ErrorDispatchers.size(); ++i)
        {
            ErrorDispatcher* const errorDispatcher = m_ErrorDispatchers[i];
            errorDispatcher->LogError(logCategory, message, file, line);
        }
    }


    ErrorDispatcherManager g_ErrorDispatcherManager;

}

DSZ_NAMESPACE_END
