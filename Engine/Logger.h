#ifndef __LOGGER_H__
#define __LOGGER_H__

#include "ProjectConfig.h"

#if defined(DSZ_USE_LOGSYSTEM)

#include <unordered_map>
#include <string>

#include "system.h"
#include "ErrorDispatcherManager.h"

DSZ_NAMESPACE_BEGIN

#define LogDebug(logCategory, messageBody) ::dsz::logger::g_ErrorDispatcherManager.LogDebug(logCategory, messageBody, __FILE__, __LINE__)
#define LogInfo(logCategory, messageBody) ::dsz::logger::g_ErrorDispatcherManager.LogInfo(logCategory, messageBody, __FILE__, __LINE__)
#define LogWarning(logCategory, messageBody) ::dsz::logger::g_ErrorDispatcherManager.LogWarning(logCategory, messageBody, __FILE__, __LINE__)
#define LogError(logCategory, messageBody) ::dsz::logger::g_ErrorDispatcherManager.LogError(logCategory, messageBody, __FILE__, __LINE__)

DSZ_NAMESPACE_END

#endif // #if defined(DSZ_USE_LOGSYSTEM)

#endif // #ifndef __LOGGER_H__