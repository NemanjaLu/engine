#pragma once

#include <Windows.h>
#include "System.h"

DSZ_NAMESPACE_BEGIN

enum class GraphicsInterface_Type { D3D12 };

class GraphicsInterface
{
public:
	virtual void Create(HWND hwnd) = 0;
	virtual void Destroy() = 0;
	//virtual void Render() = 0;
	//virtual void* GetDevice() = 0;
	//virtual void* GetDeviceContext() = 0;

	virtual void SetFullScreen(bool bFullscreen) = 0;

	virtual void BeginScene() = 0;
	virtual void EndScene() = 0;
};

DSZ_NAMESPACE_END