#include "DSZ_Math.h"

DSZ_NAMESPACE_BEGIN

XMVECTOR ToXMVector(XMFLOAT4 f)
{
	XMVECTOR v;
	memcpy((void*)&v, &f, sizeof(XMFLOAT4));
	return v;
}

XMVECTOR ToXMVector(XMFLOAT3 f)
{
	return ToXMVector(XMFLOAT4(f.x, f.y, f.z, 1.f));
}

DSZ_NAMESPACE_END