#ifndef __ERRORDISPATCHERMANAGER_H__
#define __ERRORDISPATCHERMANAGER_H__

#include "ErrorDispatcher.h"

#include <vector>

DSZ_NAMESPACE_BEGIN

namespace logger
{

    class ErrorDispatcherManager
    {
    public:
        void RegisterErrorDispatcher(ErrorDispatcher* errorDispatcher);
        void UnregisterErrorDispatcher(ErrorDispatcher* errorDispatcher);

        void LogDebug(const LogCategory& logCategory, const char* message, const char* file, int line);
        void LogInfo(const LogCategory& logCategory, const char* message, const char* file, int line);
        void LogWarning(const LogCategory& logCategory, const char* message, const char* file, int line);
        void LogError(const LogCategory& logCategory, const char* message, const char* file, int line);

    private:
        std::vector<ErrorDispatcher*> m_ErrorDispatchers;

    };

    extern ErrorDispatcherManager g_ErrorDispatcherManager;

}

DSZ_NAMESPACE_END

#endif // #ifndef __ERRORDISPATCHERMANAGER_H__