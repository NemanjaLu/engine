#include "D3D12_GraphicsInterface.h"
#include "CoreAPI.h"
#include <vector>
#include <iostream>
#include "D3D_Utils.h"
#include "D3D12_Extensions.h"

//Temp
#include "Camera.h"

#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

DSZ_NAMESPACE_BEGIN

void D3D12_GraphicsInterface::Create(HWND hwnd)
{
	PrintAdaptersInfo(std::wcout);
	std::wcout << std::endl;

	IDXGIFactory4* factory;
	CreateDXGIFactory1(__uuidof(IDXGIFactory4), (void**)&factory);

	// Device
	IDXGIAdapter* adapter = nullptr;
	D3D_FEATURE_LEVEL featureLevel = GetFeatureLevel(adapter);

	D3D12CreateDevice(NULL, featureLevel, __uuidof(ID3D12Device), (void**)&m_device);

	std::wcout << "***************** Chosen device ******************" << std::endl;
	PrintDeviceInfo(m_device, std::wcout);
	std::wcout << "**************************************************" << std::endl;
	std::wcout << std::endl;

	// Command queue
	D3D12_COMMAND_QUEUE_DESC commandQueueDesc;

	ZeroMemory(&commandQueueDesc, sizeof(commandQueueDesc));
	commandQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	commandQueueDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
	commandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	commandQueueDesc.NodeMask = 0;

	m_device->CreateCommandQueue(&commandQueueDesc, __uuidof(ID3D12CommandQueue), (void**)&m_commandQueue);

	// Swap chain
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	IDXGISwapChain* swapChain;

	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.BufferCount = 2;
	swapChainDesc.BufferDesc.Width = RES_X;
	swapChainDesc.BufferDesc.Height = RES_Y;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.Windowed = false;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.Flags = 0;
	swapChainDesc.Windowed = true;

	factory->CreateSwapChain(m_commandQueue, &swapChainDesc, &swapChain);
	swapChain->QueryInterface(__uuidof(IDXGISwapChain3), (void**)&m_swapChain);

	swapChain = 0;
	factory->Release();
	factory = 0;

	// Render target
	D3D12_DESCRIPTOR_HEAP_DESC renderTargetViewHeapDesc;
	D3D12_CPU_DESCRIPTOR_HANDLE renderTargetViewHandle;

	ZeroMemory(&renderTargetViewHeapDesc, sizeof(renderTargetViewHeapDesc));
	renderTargetViewHeapDesc.NumDescriptors = 2;
	renderTargetViewHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	renderTargetViewHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;

	m_device->CreateDescriptorHeap(&renderTargetViewHeapDesc, __uuidof(ID3D12DescriptorHeap), (void**)&m_renderTargetViewHeap);
	renderTargetViewHandle = m_renderTargetViewHeap->GetCPUDescriptorHandleForHeapStart();
	UINT renderTargetViewDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	m_swapChain->GetBuffer(0, __uuidof(ID3D12Resource), (void**)&m_backBufferRenderTarget[0]);
	m_device->CreateRenderTargetView(m_backBufferRenderTarget[0], NULL, renderTargetViewHandle);
	renderTargetViewHandle.ptr += renderTargetViewDescriptorSize;
	m_swapChain->GetBuffer(1, __uuidof(ID3D12Resource), (void**)&m_backBufferRenderTarget[1]);
	m_device->CreateRenderTargetView(m_backBufferRenderTarget[1], NULL, renderTargetViewHandle);

	m_bufferIndex = m_swapChain->GetCurrentBackBufferIndex();

	// DepthStencil buffer
	D3D12_DESCRIPTOR_HEAP_DESC dsHeapDesc;
	D3D12_CPU_DESCRIPTOR_HANDLE dsHandle;

	ZeroMemory(&dsHeapDesc, sizeof(dsHeapDesc));
	dsHeapDesc.NumDescriptors = 1;
	dsHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	dsHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	m_device->CreateDescriptorHeap(&dsHeapDesc, IID_PPV_ARGS(&m_depthStencilHeap));

	dsHandle = m_depthStencilHeap->GetCPUDescriptorHandleForHeapStart();

	DXGI_FORMAT dsFormat = DXGI_FORMAT_D32_FLOAT;

	D3D12_RESOURCE_DESC depthResourceDesc = CD3DX12_RESOURCE_DESC::Tex2D(
		dsFormat,
		RES_X,
		RES_Y,
		1,
		1);
	depthResourceDesc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	CD3DX12_CLEAR_VALUE dsClearvalue(dsFormat, 1.f, 0);

	m_device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&depthResourceDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&dsClearvalue,
		IID_PPV_ARGS(&m_depthStencilBuffer));

	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
	dsvDesc.Format = dsFormat;
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;

	m_device->CreateDepthStencilView(
		m_depthStencilBuffer,
		&dsvDesc,
		m_depthStencilHeap->GetCPUDescriptorHandleForHeapStart());

	// Command allocator
	m_device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, __uuidof(ID3D12CommandAllocator), (void**)&m_commandAllocator);
	m_device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_commandAllocator, NULL, __uuidof(ID3D12GraphicsCommandList), (void**)&m_commandList);
	m_commandList->Close();

	m_device->CreateFence(0, D3D12_FENCE_FLAG_NONE, __uuidof(ID3D12Fence), (void**)&m_fence);
	m_fenceEvent = CreateEventEx(NULL, FALSE, FALSE, EVENT_ALL_ACCESS);
	m_fenceValue = 1;

	// Temp
	CurrentCamera = new Camera();
	CurrentCamera->pos = glm::vec3(5.f, 5.f * sqrtf(2.f), 5.f);
	CurrentCamera->SetLookAt(glm::vec3(0.f), glm::vec3(0.f, 1.f, 0.f));
}

void D3D12_GraphicsInterface::Destroy() { }

void D3D12_GraphicsInterface::SetFullScreen(bool bFullscreen) { }

void D3D12_GraphicsInterface::BeginScene()
{
	D3D12_CPU_DESCRIPTOR_HANDLE renderTargetViewHandle;
	unsigned int renderTargetViewDescriptorSize;
	float color[4];

	m_commandAllocator->Reset();
	m_commandList->Reset(m_commandAllocator, m_pipelineState);

	barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	barrier.Transition.pResource = m_backBufferRenderTarget[m_bufferIndex];
	barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
	barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
	barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	m_commandList->ResourceBarrier(1, &barrier);

	renderTargetViewHandle = m_renderTargetViewHeap->GetCPUDescriptorHandleForHeapStart();
	renderTargetViewDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	if (m_bufferIndex == 1)
	{
		renderTargetViewHandle.ptr += renderTargetViewDescriptorSize;
	}

	m_commandList->OMSetRenderTargets(1, &renderTargetViewHandle, FALSE,
		&CD3DX12_CPU_DESCRIPTOR_HANDLE(m_depthStencilHeap->GetCPUDescriptorHandleForHeapStart()));

	color[0] = 100.f / 255;
	color[1] = 149.f / 255;
	color[2] = 237.f / 255;
	color[3] = 1.0;
	m_commandList->ClearRenderTargetView(renderTargetViewHandle, color, 0, NULL);
	m_commandList->ClearDepthStencilView(
		m_depthStencilHeap->GetCPUDescriptorHandleForHeapStart(),
		D3D12_CLEAR_FLAG_DEPTH,
		1.0f,
		0,
		0,
		nullptr);
	
}
void D3D12_GraphicsInterface::EndScene()
{
	barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
	barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
	m_commandList->ResourceBarrier(1, &barrier);

	m_commandList->Close();

	ID3D12CommandList* ppCommandLists[1];
	ppCommandLists[0] = m_commandList;
	m_commandQueue->ExecuteCommandLists(1, ppCommandLists);
	m_swapChain->Present(0, 0);

	WaitForPreviousFrame();
}

void D3D12_GraphicsInterface::WaitForPreviousFrame()
{
	// WAITING FOR THE FRAME TO COMPLETE BEFORE CONTINUING IS NOT BEST PRACTICE.
	// This is code implemented as such for simplicity. The D3D12HelloFrameBuffering
	// sample illustrates how to use fences for efficient resource usage and to
	// maximize GPU utilization.

	// Signal and increment the fence value.
	const UINT64 fence = m_fenceValue;
	m_commandQueue->Signal(m_fence, fence);
	m_fenceValue++;

	// Wait until the previous frame is finished.
	if (m_fence->GetCompletedValue() < fence)
	{
		m_fence->SetEventOnCompletion(fence, m_fenceEvent);
		WaitForSingleObject(m_fenceEvent, INFINITE);
	}

	m_bufferIndex = m_swapChain->GetCurrentBackBufferIndex();
}

DSZ_NAMESPACE_END