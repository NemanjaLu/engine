set GLM_VERSION=0.9.8.2
set THIRD_PARTY_DIR_NAME=ThirdParty

mkdir %THIRD_PARTY_DIR_NAME%
cd %THIRD_PARTY_DIR_NAME%

git clone https://github.com/g-truc/glm 
cd glm
git checkout tags/%GLM_VERSION%

pause