#include "editor.h"
#include <qwindow.h>
#include <EngineCore.h>
#include <enginethread.hpp>
#include <Globals.h>
#include "gamewindow.hpp"
#include "gameviewport.hpp"

using namespace dsz;

Editor::Editor(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	dsz::ConsoleCreate();

	GameWindow* gameWindow = new GameWindow();
	gameWindow->setObjectName(QStringLiteral("Game Window"));
	gameWindow->setMinimumSize(QSize(800, 600));

	GameViewport* viewport = new GameViewport();
	viewport->setObjectName("Game Viewport");
	gameWindow->setWidget(viewport);

	viewport->setFocusPolicy(Qt::StrongFocus);
	viewport->setFocus();
	viewport->setEnabled(true);

	WId wid = gameWindow->widget()->winId();
	HWND hwnd = (HWND)wid;

	this->addDockWidget(static_cast<Qt::DockWidgetArea>(1), gameWindow);


	EngineCore* core = new EngineCore();
	core->InitGraphicsInterface(GraphicsInterfaceType::D3D12, hwnd);
	EngineThread* engineThread = new EngineThread(core);
	engineThread->start();

	

	ui.centralWidget->move(QPoint(0, 0));
	this->move(QPoint(0, 0));
}

Editor::~Editor()
{
	dsz::ConsoleCleanup();
}


