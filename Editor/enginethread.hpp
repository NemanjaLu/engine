﻿#pragma once
#include <QThread>

namespace dsz { class EngineCore; }

class EngineThread : public QThread {
	Q_OBJECT

public:
	EngineThread(dsz::EngineCore* core = nullptr, QObject * parent = Q_NULLPTR);
	~EngineThread();

	dsz::EngineCore* core = nullptr;

protected:
	void run() override;

private:
	
};
