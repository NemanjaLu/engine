﻿#pragma once
#include <QWidget>

class GameViewport : public QWidget
{
	Q_OBJECT

public:
	GameViewport(QWidget * parent = Q_NULLPTR);
	~GameViewport();

protected:
	void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
	void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

private:
	bool nativeEvent(const QByteArray & eventType, void * message, long * result) Q_DECL_OVERRIDE;
};
