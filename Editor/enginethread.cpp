﻿#include "enginethread.hpp"
#include <EngineCore.h>

EngineThread::EngineThread(dsz::EngineCore* core, QObject * parent)
	: core(core), QThread(parent)
{

}

EngineThread::~EngineThread()
{

}

void EngineThread::run()
{
	if (core != nullptr)
	{
		core->Start();
	}
}