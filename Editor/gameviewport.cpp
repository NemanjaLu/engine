﻿#include "gameviewport.hpp"
#include <Windows.h>
#include <CoreAPI.h>
#include <qevent.h>

GameViewport::GameViewport(QWidget * parent) : QWidget(parent)
{

}

GameViewport::~GameViewport()
{

}

void GameViewport::keyPressEvent(QKeyEvent *event)
{
	QWidget::keyPressEvent(event);

	dsz::Input::KeyDown(event->nativeVirtualKey());
}
void GameViewport::keyReleaseEvent(QKeyEvent *event)
{
	QWidget::keyReleaseEvent(event);

	dsz::Input::KeyUp(event->nativeVirtualKey());
}


bool GameViewport::nativeEvent(const QByteArray & eventType, void * message, long * result)
{
	MSG* msg = (MSG*)message;

	dsz::WindowProc(msg->hwnd, msg->message, msg->wParam, msg->lParam);

	return QWidget::nativeEvent(eventType, message, result);
}